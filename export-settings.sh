#!/bin/bash -e

for f in .macos/*.plist; do
  [ -e "$f" ] || continue

  app=$(basename -s .plist $f)

  echo "Exporting $app settings to $f"
  defaults export $app $f
done

