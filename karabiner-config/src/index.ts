import {
  map,
  rule,
  ifDevice,
  withMapper,
  withModifier,
  writeToProfile,
} from 'karabiner.ts'

writeToProfile('Default profile', [
  norman_layout(),
  capslock(),
]);

function norman_layout() {
  let ifAppleKeyboard = ifDevice({ vendor_id: 12951 }).unless() // Not Moonlander
  return rule('Norman Layout', ifAppleKeyboard).manipulators([
    withModifier('optionalAny')([
      map('e').to('d'),
      map('r').to('f'),
      map('k').to('i'),
      map('t').to('k'),
      map('y').to('j'),
      map('i').to('r'),
      map('o').to('l'),
      map('p').to(';'),
      map('d').to('e'),
      map('f').to('t'),
      map('h').to('y'),
      map('j').to('n'),
      map('l').to('o'),
      map(';').to('h'),
      map('n').to('p'),
    ]),
  ]);
}

function capslock() {
  return rule('Change caps_lock to ctrl if pressed with other keys, to escape if pressed alone').manipulators([
    withModifier('optionalAny')([
      map('caps_lock')
        .to('left_control')
        .toIfAlone('escape')
    ]),
  ]);
}

