# karabiner-config

Rebuild the [karabiner-config](karabiner-elements.pqrs.org) into `~/.config/karabiner` using the Typescript-based configs in `src/index.ts`.

```
npm install
npm run build
```

