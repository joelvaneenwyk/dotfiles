spoon.SpoonInstall:andUse('KSheet')

spoon.ModalMgr:new('cheatsheetM')
local cmodal = spoon.ModalMgr.modal_list['cheatsheetM']

cmodal:bind('', 'escape', 'Close cheatsheet', function()
  spoon.KSheet:hide()
  spoon.ModalMgr:deactivate({'cheatsheetM'})
end)

spoon.ModalMgr.supervisor:bind('alt', '/', 'Show app cheatsheet', function()
  spoon.KSheet:show()
  spoon.ModalMgr:deactivateAll()
  spoon.ModalMgr:activate({'cheatsheetM'})
end)
