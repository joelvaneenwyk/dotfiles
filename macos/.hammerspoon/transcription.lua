-- toggle play
spoon.ModalMgr.supervisor:bind('ctrl', 'i', 'Play/Pause VLC', function()
  hs.osascript.applescript('tell application "VLC" to play')
end)

-- step backwards 1 sec
spoon.ModalMgr.supervisor:bind('ctrl', 'n', 'Rewind VLC 1 sec', function()
  hs.osascript.applescript('tell application "VLC" to step backward of 1')
end)

-- type current timestamp
spoon.ModalMgr.supervisor:bind('ctrl', 'o', 'Type VLC timestamp', function()
  result, ts, err = hs.osascript.applescript([[
    tell application "VLC" to set VLCTIME to (current time as integer)
    set MIN to (VLCTIME div 60 as integer)
    set SEC to (VLCTIME - (MIN * 60) as integer)
    if MIN < 10 then set MIN to 0 & MIN
    if SEC < 10 then set SEC to 0 & SEC
    set TC to (("[" & MIN & ":" & SEC & "]") as text)

    return TC
  ]])

  if result then
    hs.eventtap.keyStrokes(ts)
  end
end)
