function yabai(args, completion)
  local yabai_task = hs.task.new('/opt/homebrew/bin/yabai', function(err, stdout, stderr)
    print('stdout: '.. stdout, 'stderr: ' .. stderr)
    if type(completion) == 'function' then
      completion(stdout, stderr)
    end
  end, args)

  yabai_task:start()
end

function yc(args, completion)
  yabai(args, completion)
  spoon.ModalMgr:deactivate({'yabaiM'})
end

spoon.ModalMgr:new('yabaiM')
local cmodal = spoon.ModalMgr.modal_list['yabaiM']

cmodal:bind('', 'escape', 'Exit yabaiM', function() spoon.ModalMgr:deactivate({'yabaiM'}) end)
cmodal:bind('', 'tab', 'Toggle Cheatsheet', function() spoon.ModalMgr:toggleCheatsheet() end)

cmodal:bind('', 'return', 'Swap window to largest region', function() yc({ '-m', 'window', '--swap', 'largest' }) end)
cmodal:bind('', 'e', 'Toggle split type', function() yc({ '-m', 'window', '--toggle', 'split' }) end)
cmodal:bind('', 'd', 'Toggle zoom parent', function() yc({ '-m', 'window', '--toggle', 'zoom-parent' }) end)
cmodal:bind('', 'f', 'Toggle fullscreen', function() yc({ '-m', 'window', '--toggle', 'zoom-fullscreen' }) end)
cmodal:bind('', 'space', 'Toggle float', function()
  yabai({ '-m', 'window', '--toggle', 'float' })
  yabai({ '-m', 'window', '--grid', '4:4:1:1:2:2' })
  spoon.ModalMgr:deactivate({'yabaiM'})
end)

local left = 'n'
local right = 'o'
local center = 'i'
local up = 'r'
local down = ','

cmodal:bind('', left, 'Focus west', function() yabai({ '-m', 'window', '--focus', 'west' }) end)
cmodal:bind('', down, 'Focus south', function() yabai({ '-m', 'window', '--focus', 'south' }) end)
cmodal:bind('', up, 'Focus north', function() yabai({ '-m', 'window', '--focus', 'north' }) end)
cmodal:bind('', right, 'Focus east', function() yabai({ '-m', 'window', '--focus', 'east' }) end)

cmodal:bind('ctrl', left, 'Swap west', function() yabai({ '-m', 'window', '--warp', 'west' }) end)
cmodal:bind('ctrl', down, 'Swap south', function() yabai({ '-m', 'window', '--warp', 'south' }) end)
cmodal:bind('ctrl', up, 'Swap north', function() yabai({ '-m', 'window', '--warp', 'north' }) end)
cmodal:bind('ctrl', right, 'Swap east', function() yabai({ '-m', 'window', '--warp', 'east' }) end)

cmodal:bind('shift', left, 'Grow horizontally', function()
  yabai({ '-m', 'window', '--resize', 'left:-20:0' })
  yabai({ '-m', 'window', '--resize', 'right:-20:0' })
end)
cmodal:bind('shift', down, 'Grow vertically', function()
  yabai({ '-m', 'window', '--resize', 'bottom:0:20' })
  yabai({ '-m', 'window', '--resize', 'top:0:20' })
end)
cmodal:bind('shift', up, 'Shrink vertically', function()
  yabai({ '-m', 'window', '--resize', 'top:0:-20' })
  yabai({ '-m', 'window', '--resize', 'bottom:0:-20' })
end)
cmodal:bind('shift', right, 'Shrink horizontally', function()
  yabai({ '-m', 'window', '--resize', 'right:20:0' })
  yabai({ '-m', 'window', '--resize', 'left:20:0' })
end)

cmodal:bind('shift', center, 'Balance windows', function() yabai({ '-m', 'space', '--balance' }) end)

cmodal:bind('', 's', 'Stack window', function()
  yc({ '-m', 'query', '--windows', '--window' }, function(stdout, stderr)
    data = hs.json.decode(stdout)
    wid = data.id

    print('found window ' .. wid)
    yabai({ '-m', 'window', 'west', '--stack', tostring(wid) })
  end)
end)

cmodal:bind('shift', 's', 'Unstack window', function()
  yabai({ '-m', 'window', '--toggle', 'float' })
  yabai({ '-m', 'window', '--toggle', 'float' })
end)

spoon.ModalMgr.supervisor:bind('alt', 'e', 'Enter yabaiM', function()
  spoon.ModalMgr:deactivateAll()
  spoon.ModalMgr:activate({'yabaiM'}, '#74BB67')
end)
