-- send the next quote from my notes to my phone as a notification using MacroDroid
local log = hs.logger.new('quotes', 'debug')

function trim(s)
  return s:match'^()%s*$' and '' or s:match'^%s*(.*%S)'
end

function sendNote(path, tag, lastItem)
  if noteNotificationUrl == nil or noteNotificationUrl == '' then
    hs.alert.show('noteNotificationUrl needs to be defined in config.lua')
    return
  end

  log.d('looking in ' .. path)

  local output, _, _, rc = hs.execute("grep -l -r '" .. tag .. "' " .. path .. " | sort")
  if rc == 0 then
    local lines = hs.fnutils.split(output, '\n')
    log.d('found ' .. #lines .. ' notes')

    local lastItem = hs.settings.get(tag)
    if lastItem == nil then
      lastItem = -1
    end

    local nextItem = math.fmod(lastItem + 1, #lines)

    log.d('last item ' .. lastItem .. ' next item ' .. nextItem)

    local f = io.open(lines[nextItem + 1], 'r')
    local text = f:read('*all')
    f:close()

    local message = trim(text:gsub(tag, ''))
    local headers = {
      ["Content-Type"] = "text/plain; charset=utf-8",
    }
    log.d('note: ' .. message)

    hs.http.asyncPost(noteNotificationUrl .. '?title=' .. hs.http.encodeForQuery(tag), message, headers, function(code, body)
      log.d('notification result ' .. code .. ': ' .. body)

      if code ~= 200 then
        hs.alert.show(body)
      else
        hs.settings.set(tag, nextItem)
      end
    end)
  end
end

hs.urlevent.bind('sendPromise', function()
  sendNote('~/Documents/Notes/Zettelkasten', '#promises')
end)

hs.urlevent.bind('sendQuote', function()
  sendNote('~/Documents/Notes/Zettelkasten', '#quotes')
end)

