-- type clipboard contents to get around paste problems
spoon.ModalMgr.supervisor:bind({'shift', 'alt'}, 'v', 'Type contents of clipboard', function()
  contents = hs.pasteboard.getContents()
  hs.eventtap.keyStrokes(contents)
end)
