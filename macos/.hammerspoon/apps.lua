spoon.ModalMgr:new('appM')
local cmodal = spoon.ModalMgr.modal_list['appM']

cmodal:bind('', 'escape', 'Exit appM', function() spoon.ModalMgr:deactivate({'appM'}) end)
cmodal:bind('', 'tab', 'Toggle Cheatsheet', function() spoon.ModalMgr:toggleCheatsheet() end)

hsapp_list = {
  {key = 'f', name = 'Finder', applescript = 'tell application "Finder" to make new Finder window to home\ntell application "Finder" to activate'},
  {key = 's', name = 'Orion RC'},
  {key = 't', name = 'Ghostty'},
  {key = 'n', name = 'FSNotes'},
  {key = 'v', id = 'com.apple.ActivityMonitor'},
  {key = 'y', id = 'com.apple.systempreferences'},
}

for _, v in ipairs(hsapp_list) do
  if v.cmd then
    cmodal:bind('', v.key, v.name, function()
      hs.execute(v.cmd)
      spoon.ModalMgr:deactivate({'appM'})
    end)
  elseif v.applescript then
    cmodal:bind('', v.key, v.name, function()
      hs.osascript.applescript(v.applescript)
      spoon.ModalMgr:deactivate({'appM'})
    end)
  elseif v.id then
    local located_name = hs.application.nameForBundleID(v.id)
    if located_name then
      cmodal:bind('', v.key, located_name, function()
        hs.application.launchOrFocusByBundleID(v.id)
        spoon.ModalMgr:deactivate({'appM'})
      end)
    end
  elseif v.name then
    cmodal:bind('', v.key, v.name, function()
      hs.application.launchOrFocus(v.name)
      spoon.ModalMgr:deactivate({'appM'})
    end)
  end
end

spoon.ModalMgr.supervisor:bind('alt', 'a', 'Enter appM', function()
  spoon.ModalMgr:deactivateAll()
  spoon.ModalMgr:activate({'appM'}, '#FFBD2E')
end)
