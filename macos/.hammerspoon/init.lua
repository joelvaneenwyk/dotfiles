-- load configs
require('config')

hyper = {'ctrl', 'alt', 'shift', 'command'}
meh = {'ctrl', 'alt', 'shift'}

hs.hotkey.alertDuration = 0.25
hs.hints.showTitleThresh = 0
hs.window.animationDuration = 0

-- install spoons automatically
hs.loadSpoon('SpoonInstall')

-- reload config on change
spoon.SpoonInstall:andUse('ReloadConfiguration', {
  start = true
})

-- initialize modal manager
spoon.SpoonInstall:andUse('ModalMgr')

spoon.ModalMgr.supervisor:bind('alt', 'L', 'Lock Screen', function() hs.caffeinate.lockScreen() end)
spoon.ModalMgr.supervisor:bind('alt', 'Z', 'Toggle Hammerspoon Console', function() hs.toggleConsole() end)

require('apps')
require('bible')
require('bluetooth')
require('browserurl')
require('cheatsheet')
require('clipboard')
require('notes')
require('transcription')
require('yabai')

spoon.ModalMgr.supervisor:enter()

-- initialize stackline
stackline = require "stackline"
stackline:init()

