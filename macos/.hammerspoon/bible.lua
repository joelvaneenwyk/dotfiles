local inspect = require('inspect')

function esv(ref, cb)
  local url = 'https://api.esv.org/v3/passage/text/?include-headings=false&include-footnotes=false&include-footnote-body=false&indent-paragraphs=0&indent-poetry=false&include-verse-numbers=false&q=' .. hs.http.encodeForQuery(ref)
  local headers = {}
  headers['Authorization'] = 'Token 39f0cce15dcf9acba8ecdcda9e38b84e3146d774'

  hs.http.asyncGet(url, headers, function(status, body, headers)
    if status == 200 then
      local data = hs.json.decode(body)
      local text = data['passages'][1]
      local lines = {}
      for s in text:gmatch('[^\r\n]+') do
        table.insert(lines, s)
      end

      local fulltext = table.concat(lines, '\n', 2)

      cb(fulltext:gsub('%(ESV%)', ''))
    end
  end)
end

function csb(ref, cb)
  local url = 'https://www.biblegateway.com/passage/?search=' .. hs.http.encodeForQuery(ref) .. '&version=CSB'

  hs.http.asyncGet(url, {}, function(status, body, headers)
    if status == 200 then
      local text = body:match('<meta property="og:description" content="([^\"]*)"')

      cb(text)
    end
  end)
end

function lookupVerse(version, versionCb)
  local ref = hs.pasteboard.getContents()
  ref = string.sub(ref, 1, 30)

  hs.alert.show(ref)

  versionCb(ref, function(text)
    hs.alert.show(text)

    local withref = text .. '\n-- ' .. ref .. ' (' .. version .. ')'

    hs.eventtap.keyStrokes(withref)
  end)
end

-- lookup the bible reference in the clipboard and type into the window
spoon.ModalMgr.supervisor:bind({'shift', 'alt'}, 'b', 'Lookup Bible Reference (CSB)', function()
  lookupVerse('CSB', csb)
end)

spoon.ModalMgr.supervisor:bind({'shift', 'alt', 'ctrl'}, 'b', 'Lookup Bible Reference (ESV)', function()
  lookupVerse('ESV', esv)
end)
