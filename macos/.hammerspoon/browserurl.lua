-- type current browser url in markdown format
spoon.ModalMgr.supervisor:bind('alt', 'v', 'Type Browser Link', function()
  local safari_running = hs.application.applicationsForBundleID('com.apple.Safari')
  local chrome_running = hs.application.applicationsForBundleID('com.google.Chrome')
  if #safari_running > 0 then
    local stat, data = hs.applescript("tell application 'Safari' to get {URL, name} of current tab of window 1")
    if stat then hs.eventtap.keyStrokes('[' .. data[2] .. '](' .. data[1] .. ')') end
  elseif #chrome_running > 0 then
    local stat, data = hs.applescript("tell application 'Google Chrome' to get {URL, title} of active tab of window 1")
    if stat then hs.eventtap.keyStrokes('[' .. data[2] .. '](' .. data[1] .. ')') end
  end
end)

function appID(app)
  return hs.application.infoForBundlePath(app)['CFBundleIdentifier']
end

chromeBrowser = appID('/Applications/Google Chrome.app')
orionBrowser = appID('/Applications/Orion RC.app')
safariBrowser = appID('/Applications/Safari.app')

spoon.SpoonInstall:andUse("URLDispatcher", {
  config = {
    url_patterns = {
      { "https?://meet%.google%.com", chromeBrowser },
      { "msteams:",                   "com.microsoft.teams2" },
      { "https?://.*zoom.us/j/",      "us.zoom.xos" },
    },
    url_redir_decoders = {
      -- Send MS Teams URLs directly to the app
      { "MS Teams URLs", "(https://teams.microsoft.com.*)", "msteams:%1", true },
    },
    default_handler = orionBrowser
  },
  start = true,
  -- Enable debug logging if you get unexpected behavior
  loglevel = 'debug'
})
