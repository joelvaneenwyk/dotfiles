# init brew
if test -d /opt/homebrew
    /opt/homebrew/bin/brew shellenv | source
end

# install plugins
fundle plugin 'fisherman/getopts'
fundle plugin 'PatrickF1/fzf.fish'
fundle plugin 'edc/bass'
fundle init

# setup prompt
starship init fish | source

# suppress fish greeting
set -x fish_greeting ""

# update path
if test -d $HOME/.local/bin
    fish_add_path $HOME/.local/bin
end

if test -d $HOME/anaconda3/bin
    fish_add_path $HOME/anaconda3/bin
end

if test -d ./node_modules/bin
   fish_add_path ./node_modules/.bin
end

if test -d /usr/local/opt/openssl@1.1/bin
   fish_add_path /usr/local/opt/openssl@1.1/bin
end

if test -d /opt/local/bin
   fish_add_path /opt/local/bin
end

set -x ANDROID_SDK_ROOT $HOME/Library/Android/Sdk
if test -d $ANDROID_SDK_ROOT
   fish_add_path $ANDROID_SDK_ROOT/platform-tools
end

if test -d /usr/local/opt/openssl@1.1/bin
    fish_add_path /usr/local/opt/openssl@1.1/bin
end

if test -d /c/tools/nvim-win64/bin
    fish_add_path /c/tools/nvim-win64/bin
end

if test -d /c/Users/jdve/AppData/Local/Android/Sdk/platform-tools
    fish_add_path /c/Users/jdve/AppData/Local/Android/Sdk/platform-tools
end

if test -d /c/ProgramData/chocolatey/bin
    fish_add_path /c/ProgramData/chocolatey/bin
end

if test -d $HOME/.cargo/bin
    fish_add_path $HOME/.cargo/bin
end

set brew (brew --prefix)
set -gx LESSOPEN "|$brew/bin/lesspipe.sh %s"

# color stderr in red
if [ (uname) = "Darwin" ]
    set -gx LD_PRELOAD "$HOME/.local/lib/libstderred.so"
end

# use 1Password ssh agent
set -gx SSH_AUTH_SOCK ~/Library/Group\ Containers/2BUA8C4S2C.com.1password/t/agent.sock

# set path to zk notebook
if test -d /c/Users/jdve/Documents/Notes/Zettelkasten
    set -gx ZK_NOTEBOOK_DIR /c/Users/jdve/Documents/Notes/Zettelkasten
end

if test -d "$HOME/Documents/Notes/Zettelkasten"
    set -gx ZK_NOTEBOOK_DIR "$HOME/Documents/Notes/Zettelkasten"
end

# aliases
alias more="less -r"
alias less="less -r"
alias ll="ls -l"
alias grep="grep --color=always"
alias rg="rg --smart-case"

# git aliases
alias gst='git status'
alias gaa='git add -A'
alias gc='git commit'
alias gcm='git checkout main'
alias gd='git diff'
alias gdc='git diff --cached'
alias co='git checkout'

# [f]uzzy check[o]ut
function fo
  git branch --no-color --sort=-committerdate --format='%(refname:short)' | fzf --header 'git checkout' | xargs git checkout
end

alias up='git push'
alias upf='git push --force'
alias pu='git pull'
alias pur='git pull --rebase'
alias fe='git fetch'
alias re='git rebase'
alias lr='git l -30'
alias cdr='cd $(git rev-parse --show-toplevel)' # cd to git Root
alias hs='git rev-parse --short HEAD'
alias hm='git log --format=%B -n 1 HEAD'

# use eza if available
if type -q eza
    alias ls="eza --git --time-style=iso"
else
    echo "Pro Tip: install eza"
end

# use bat if available
if type -q bat
    alias cat="bat"
    set -gx BAT_THEME "Monokai Extended"
end

alias lt="ll --tree"
alias la="ls -a"

# use asdf if available
if test -z $ASDF_DATA_DIR
    set _asdf_shims "$HOME/.asdf/shims"
else
    set _asdf_shims "$ASDF_DATA_DIR/shims"
end

if not contains $_asdf_shims $PATH
    set -gx --prepend PATH $_asdf_shims
end
set --erase _asdf_shims

# use grc if available
if test -f /usr/local/etc/grc.fish
    source /usr/local/etc/grc.fish
end

# use zoxide if available
if type -q zoxide
    zoxide init fish | source
end

# set editor
if type -q nvim
    alias vi="nvim"
    alias vim="nvim"

    set -gx EDITOR (which nvim)
else
    set -gx EDITOR (which vim)

    echo "Pro Tip: install neovim"
end

# configure fzf
if type -q fzf
    set -gx FZF_FIND_FILE_COMMAND "fd --type f . \$dir"
    set -gx FZF_CTRL_T_COMMAND "fd --type f . \$dir"
    set -gx FZF_DEFAULT_OPTS (echo $FZF_DEFAULT_OPTS | tr -d '\n')
else
    echo "Pro Tip: install fzf"
end

# enable activating anaconda environments
if type -q conda
    source (conda info --root)/etc/fish/conf.d/conda.fish
end

# enable git commit signing
if type -q gpg
    git config --global commit.gpgsign true
else
    git config --global --unset commit.gpgsign
end

