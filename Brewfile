# Provides Espanso text expansion packages for automated typing.
tap "espanso/espanso"

# Felix Kratz formulae for advanced macOS enhancements.
tap "FelixKratz/formulae"

# Koekeishiya formulae for powerful tiling window management.
tap "koekeishiya/formulae"

# Jorge L. personal tap for specialized macOS utilities.
tap "jorgelbg/tap"

##
## source code management
##

# A modern DVCS with simpler workflows than Git.
brew "jj"

##
## cli helpers
##

# Mac App Store CLI for automating app installs.
brew "mas"

# Universal runtime version manager (Node, Ruby, etc.).
brew "asdf"

# Cat clone with syntax highlighting and Git integration.
brew "bat"

# Friendly CLI DNS client, an alternative to dig.
brew "doggo"

# Better du, displays disk usage with a clean interface.
brew "dust"

# Modern replacement for ls with color and icons.
brew "eza"

# User-friendly alternative to find with simpler syntax.
brew "fd"

# Fish shell provides smart suggestions and user-friendly scripts.
brew "fish"

# Command-line fuzzy finder for speedy searching.
brew "fzf"

# Adds color to console output for better readability.
brew "grc"

# Lightweight, flexible JSON processor for the CLI.
brew "jq"

# Easy command runner for project-specific workflows.
brew "just"

# Vim-based editor with modern extensibility.
brew "neovim"

# Lightning-fast text search in files.
brew "ripgrep"

# A modern ps replacement with a neat UI.
brew "procs"

# Easy, intuitive find & replace CLI tool.
brew "sd"

# Minimal, fast, customizable prompt showing contextual info.
brew "starship"

# Manages symlinks, perfect for organizing dotfiles.
brew "stow"

# Simplified man pages with practical examples.
brew "tldr"

##
## window management
##

# Tiling window manager for macOS with deep scriptability.
brew "koekeishiya/formulae/yabai"

# Adds customizable borders to macOS windows.
brew "FelixKratz/formulae/borders"

##
## macos tools
##

# Automate macOS tasks via Lua scripts and hotkeys.
cask "hammerspoon"

# Minimal calendar that sits in your menu bar.
cask "itsycal"

# Privacy-focused VPN with a simple pricing model.
# joelvaneenwyk - Disabled
#cask "mullvadvpn"

# macOS client for Google’s Nearby Share protocol.
cask "grishka/grishka/neardrop"

# Graphical interface for Neovim with better rendering.
cask "neovide"

##
## productivity
##

# Secure password manager for generating and storing credentials.
cask "1password"

# Text expansion app with GUI for quick snippet management.
cask "espanso"

# Developer font with icons for better terminal visuals.
cask "font-jetbrains-mono-nerd-font"

# Modern markdown note manager with iCloud sync.
cask "fsnotes"

# Keyboard-centric macOS email client with powerful search.
cask "mailmate"

# Customizable launcher for speedier workflows on macOS.
cask "raycast"

# Fast, encrypted file sync across your devices.
cask "resilio-sync"

# Cross-platform to-do manager with a clean interface.
# joelvaneenwyk - 2025-03-06 Not using. "Transitioned" to Obsidian.
#cask "todoist"

# Minimalist Markdown editor with real-time rendering.
# joelvaneenwyk - 2025-03-06 Not using. "Transitioned" to Sublime.
#cask "typora"

##
## dev tools
##

# Cross-platform terminal emulator // https://ghostty.org
cask "ghostty"

# GUI for browsing and testing APIs quickly.
cask "rapidapi"

# Extensible code editor with a large plugin ecosystem.
cask "visual-studio-code"

##
## media tools
##

# Modern media player for macOS based on mpv.
cask "iina"

# Photo organizer with robust syncing across devices.
# joelvaneenwyk - 2025-03-06 Not currently using on macOS.
#cask "mylio"
