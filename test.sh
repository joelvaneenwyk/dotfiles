#!/bin/bash --login -eo pipefail

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

echo $SCRIPT_DIR

if [ -t 0 ]; then
  echo "interactive"
else
  echo "non-interactive"
fi


