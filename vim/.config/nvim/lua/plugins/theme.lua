return {
  "EdenEast/nightfox.nvim",
  config = function ()
    require("nightfox").setup({
      options = {
        transparent = true,
        colorblind = {
          enable = true,
          severity = {
            protan = 1.0,
            deutan = 0.9,
            tritan = 0.3
          },
        },
      },
    })

    vim.cmd("colorscheme carbonfox")
  end
}

